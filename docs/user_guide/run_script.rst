#############
Run Script
#############

***********************
Example Run Scripts
***********************


A run script needs to initialize your wrapper, load your configuration, and run your optimization.

Single Objective Optimization Run Scripts
===========================================

.. rli:: https://raw.githubusercontent.com/jemissik/fetch3_nhl/develop/fetch3/optimize/run_optimization.py
    :language: python

link to source: https://github.com/jemissik/fetch3_nhl/blob/develop/fetch3/optimize/run_optimization.py


.. rli:: https://raw.githubusercontent.com/madeline-scyphers/palm_wrapper/main/palm_wrapper/optimize/run.py
    :language: python

link to source: https://github.com/madeline-scyphers/palm_wrapper/blob/main/palm_wrapper/optimize/run.py

