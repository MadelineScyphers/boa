########
Examples
########

.. toctree::
    :maxdepth: 2

    example_py_run
    optimization_run
    load_scheduler
    cached_notebooks/example_optimization_results